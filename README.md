# Tienda CCE
![](https://pandao.github.io/editor.md/images/logos/editormd-logo-180x180.png)

[TOC]

## Definición del alcance

La definición del alcance esta definido por el cliente en los detalles de la prueba y los divideremos en el alcance funcional y no funcional, adicionando los 

### Alcance funcional

Se tiene una tienda con N cantidad de **productos**, los cuales se podrán ***añadir, eliminar, actualizar*** en el carro de compras y en caso de que haya añadido toda la cantidad de un producto, al intentar añadir de nuevo ese producto deberá indicarle al usuario que no hay disponibilidad de ese producto. Cada usuario podrá hacer un ***registro básico*** (nombre/email/password) para poder ***realizar la compra*** de dichos productos.

Se requiere la implementación de un API que permita mínimo:

✓ Listar los productos
✓ Añadir un producto al carro
✓ Actualizar un producto existente en el carro
✓ Eliminar un producto del carro
✓ Validar el stock del producto, en caso que llegue a 0 deberá informar

### Desglose del alcance funcional
1. Realizar el registro del usuario en el API
1. Listar productos
1. Agregar producto al carrito del usuario
	1. Informar al usuario cuando no exista producto disponible
1. Eliminar producto del carrito
1. Actualizar un producto existente

### Requerimientos no funcionales

1. El código debe estar debidamente comentado
1. Esta prueba puede ser desarrollada en el lenguaje de programación que usted elija y sienta que tiene mayor conocimiento. 
1. No hay restricciones en el framework o tecnología de almacenamiento de datos. 
1. Se espera documentación de cómo fue abordada la solución, con información del diseño y/o patrones implementados
1. las instrucciones necesarias para montar el proyecto y probar las funcionalidades requeridas, por ejemplo, migraciones de base de datos, esquemas de base de datos, colecciones en Postman, etc...
1. Se tendrá en alta estima el uso de buenas prácticas de desarrollo según el lenguaje y la escritura de pruebas automatizadas determinísticas

### Nice to have ( suma puntos en orden de prioridad)
✓ Pruebas unitarias
✓ Autenticación de usuario

## Diseño de la solución

El problema expuesto por el cliente es un problema comun en muchos sectores productivos con diferentes escenarios de aplicación. En este caso vamos a realizar la funcionalidad minima solicitada por el cliente (porque es una prueba técnica), primero abordaremos la solución para revisar que cumpla con los requerimientos funcionales y posteriormente realizaremos la definición de la tecnologia a aplicar para llegar al proceso de codificación

### Principios de diseño

**KISS**: Vamos a mantener esta solucion lo mas simple en su implementación
**Single Responsibility**: Vamos a mantener divididas las responsabilidades de los componentes del software

### Atributos de calidad

Desplegabilidad (facilidad de despliegue).
Modificabilidad.
Testeabilidad (facilidad de probar el sistema).

### Analisis funcional

En el desglose del alcance funcional se identifico claramente que se neceita un API con 5 funcionalidades, vamos a crear una capa Controller para identificar claramente esas funciones los encargados de procesar las entradas de usuario y realizar validaciones se lo dejaremos a la capa logica y crearemos una capa de persistencia de datos para almacenar la información

![Arquitectura](images/Arquitectura.png)

#### Consideraciones

Al momento de plantear el diseño empezamos a crear unas nuevas consideraciones que ayudaran a entender la solucion al problema propuesto:

1. El token de usuario sera el mismo id de usuario, en proximas iteraciones se usaran metodologías de generación de token como JWT
1. El carrito de compra se almacenara con el id del usuario y un usuario puede tener 0(cero) o 1(uno) carrito de compra en estado "En proceso"
1. Como es un api que no tienen interfaz visual vamos a responder en las acciones de la tienda el carrito completo


### Adopción de tecnologias

Para facilitar el despliegue vamos a usar **Springboot**, lo que implica que nos iremos por un lenguaje JAVA.
Para facilitar la consulta del API y evitar el uso de herramientas adicionales implementaremos **Swagger**, aqui podemos probar el API
Para facilitar el despliegue usaremos una base de datos temporal (H2 Database) y pensando en esto debemos cargar una información basica al momento del despliegue

![Springboot](https://th.bing.com/th/id/OIP.h2s6UPlKpYe09HtS-d7BhwAAAA?pid=ImgDet&rs=1 "Springboot")

![swagger](https://s3-us-west-2.amazonaws.com/assertible/integrations/swagger-logo-horizontal.jpeg "swagger")

![Toolsiute](http://www.digitizedpost.com/wp-content/uploads/2021/02/spring-tool-suite-version-4-ide-launching.png "Toolsiute")

## Codificación

El proceso de codificación tomo medio día pero al final se logro

![codificacion](images/Codificacion.JPG "codificacion")

## Como validar la solución

### Primera opción (IDE)

Para descargar el proyecto a un IDE de eclipse, debes tener eclipse instalado e importar el proyecto, una vez descargado configuras y refrescas graddle para que todo te aparezca sin errores, Y finalizas con un run.

![IDE](images/IDE.JPG "IDE")

### Segunda Opcion

Ingresas a la carpeta Jar del codigo y descargas el archivo tienda.jar, si tienes instalado java en tu equipo solo es dar doble click.

### Ingresar al API

Facil presiona el siguiente link http://localhost:8033/swagger-ui/index.html

![API](images/API.JPG "API")

### Ahora las pruebas

1. Login para poder conocerte, 3 usuarios (U1, U2, U3 todos con clave 123)
![e1](images/EndPoint1.JPG "e1")
![er1](images/EndPoint1_Response.JPG "er1")
1. Consulta el catalogo
![e2](images/EndPoint2.JPG "e2")
1. Agrega dos productos
![e3](images/EndPoint3.JPG "e3")
![er3](images/EndPoint3_Response.JPG "er3")
1. Modifica el primer producto
![e4](images/EndPoint4.JPG "e4")
![er4](images/EndPoint4_Response.JPG "er4")
1. Elimina el segundo
![e5](images/EndPoint5.JPG "e5")
![er5](images/EndPoint5_Response.JPG "er5")
1. Aunque siempre ves como resultado el carrito, vuelvelo a ver
![e6](images/EndPoint6.JPG "e6")
![er6](images/EndPoint6_Response.JPG "er6")
1. Ahora finaliza la compra y contratame !! :smile: :smile:
![e7](images/EndPoint7.JPG "e7")
![er7](images/EndPoint7_Response.JPG "er7")

### Muchas gracias
Les quedo debiendo las pruebas, feliz día
