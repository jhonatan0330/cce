# Ahora esto es una prueba de modificar HU
____
# CEPAS

> **C**omponente de **E**jecucion de **P**ruebas **A**utomaticas del **S**IIF Nacion

## CONTEXTO 

Hola
### ALCANCE
CEPAS es un proyecto de software que nace de las siguientes necesidades:

#### 1. Requerimiento Prueba diaria
Permitir generar un informe de las ejecuciones diarias de pruebas automaticas que contenga el detalle de las transacciones que se ejecutan

#### REQ001 Requerimiento Ejecución por todo el grupo de QA {#custom-id}
Permitir ejecutar una transacción de forma automatica, usando los programas de automatización que se van diseñando

## VISION FUTURA

Esperamos que en un periodo corto de tiempo podamos incluir a CEPAS las siguientes funcionalidades

## RESTRICCIONES 

[Requerimiento 1](/CONTEXTO)


### My Multi Word Header

## CONTEXTO 

 

## PRINCIPIOS 

## DRIVERS 


## ATRIBUTOS DE CALIDAD 


## REQUERIMIENTOS FUNCIONALES PRINCIPALES 


## REQUERIMIENTOS NO FUNCIONALES 
 

## DECISIONES DE ARQUITECTURA 

 

## Getting Started

Welcome to the VS Code Java world. Here is a guideline to help you get started to write Java code in Visual Studio Code.

## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies

Meanwhile, the compiled output files will be generated in the `bin` folder by default.

> If you want to customize the folder structure, open `.vscode/settings.json` and update the related settings there.

## Dependency Management

[click on this link](#requerimiento-prueba-diaria)

[click on this link](#contexto)

[click on this link](#custom-id)

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |


```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

Gone camping! :tent: Be back soon.

That is so funny! :joy:
