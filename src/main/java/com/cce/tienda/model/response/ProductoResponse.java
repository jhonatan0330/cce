package com.cce.tienda.model.response;

import com.cce.tienda.model.dto.ProductoDTO;

public class ProductoResponse {

	private String id;
	private String nombre;
	private int existencias;

	public ProductoResponse() {
		super();
	}
	
	public ProductoResponse(ProductoDTO dto) {
		super();
		id = dto.getId();
		nombre = dto.getNombre();
		existencias = dto.getExistencias();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getExistencias() {
		return existencias;
	}
	public void setExistencias(int existencias) {
		this.existencias = existencias;
	}

}
