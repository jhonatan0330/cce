package com.cce.tienda.model.response;


public class TokenResponse {
	
	public TokenResponse(String token) {
		super();
		this.token = token;
	}

	private String token;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
