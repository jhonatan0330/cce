package com.cce.tienda.model.response;

import java.util.List;

import com.cce.tienda.model.dto.CompraDTO;

public class CarritoResponse {
	
	private int items;
	private String codigo;
	private List<ProductoCompradoResponse> productos;
	private String estado;
	
	public CarritoResponse() {
		super();
	}
	
	public CarritoResponse(CompraDTO dto) {
		super();
		codigo = dto.getId();
		items = dto.getCantidadItems();
		estado = dto.getEstado();
	}
	
	public int getItems() {
		return items;
	}
	public void setItems(int items) {
		this.items = items;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public List<ProductoCompradoResponse> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductoCompradoResponse> productos) {
		this.productos = productos;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
