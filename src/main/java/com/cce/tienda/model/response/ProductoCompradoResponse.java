package com.cce.tienda.model.response;

import com.cce.tienda.model.dto.ProductoCompradoDTO;

public class ProductoCompradoResponse {

	private String producto;
	private String idProducto;
	private int cantidad;

	public ProductoCompradoResponse() {
		super();
	}
	
	public ProductoCompradoResponse(ProductoCompradoDTO dto) {
		super();
		idProducto = dto.getIdProducto();
		producto = dto.getIdCompra();
		cantidad = dto.getCantidad();
	}
	
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}
