package com.cce.tienda.model.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity	
public class DatosSesionDTO {

	@Id
	@GenericGenerator(name = "sequence_id", strategy = "com.cce.tienda.model.IdGenerator")
    @GeneratedValue(generator = "sequence_id")
	private String id;
	private String idUsuario;
	private String username;
	private String clave;
	
	public DatosSesionDTO(String username, String idUsuario) {
		super();
		this.username = username;
		this.idUsuario = idUsuario;
		this.clave = "123";
	}
	
	public DatosSesionDTO() {
		super();
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	
}
