package com.cce.tienda.model.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class UsuarioDTO {

	@Id
	@GenericGenerator(name = "sequence_id", strategy = "com.cce.tienda.model.IdGenerator")
    @GeneratedValue(generator = "sequence_id")
	private String id;
	
	private String nombre;
	
	public UsuarioDTO() {
		super();
	}
	
	public UsuarioDTO(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
