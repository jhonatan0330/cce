package com.cce.tienda.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cce.tienda.exception.TiendaException;
import com.cce.tienda.model.response.ProductoResponse;
import com.cce.tienda.persistence.ProductoRepository;

@Service
public class InventarioService {
	
	@Autowired private ProductoRepository productoRepository;
	
	public List<ProductoResponse> listarInventario() throws TiendaException{
		return productoRepository.findAll().stream().map(x ->new ProductoResponse(x)).toList();
	}
	
}
