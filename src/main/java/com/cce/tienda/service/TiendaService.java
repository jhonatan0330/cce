package com.cce.tienda.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cce.tienda.exception.TiendaException;
import com.cce.tienda.model.CompraState;
import com.cce.tienda.model.dto.CompraDTO;
import com.cce.tienda.model.dto.ProductoCompradoDTO;
import com.cce.tienda.model.dto.ProductoDTO;
import com.cce.tienda.model.request.ProductoRequest;
import com.cce.tienda.model.response.CarritoResponse;
import com.cce.tienda.model.response.ProductoCompradoResponse;
import com.cce.tienda.persistence.CompraRepository;
import com.cce.tienda.persistence.ProductoCompradoRepository;
import com.cce.tienda.persistence.ProductoRepository;

@Service
public class TiendaService {
	
	@Autowired CompraRepository compraRepository;
	@Autowired ProductoCompradoRepository productoCompradoRepository;
	@Autowired ProductoRepository productoRepository;

	/**
	 * Agregamos un producto al carrito de compra
	 * Si no ha abierto carrito le abrimos uno
	 * Si ya tenia de ese producto lo sumamos
	 * Inventariamos el producto
	 * @param producto
	 * @param usuario
	 * @return
	 * @throws TiendaException
	 */
	public CarritoResponse agregarProducto(ProductoRequest producto, String usuario) throws TiendaException {
		CompraDTO compra = compraActiva(usuario);
		if(compra==null) {
			compra = new CompraDTO();
			compra.setCantidadItems(0);
			compra.setIdUsuario(usuario);
			compra.setFechaInicio(new Date());
			compra.setEstado(CompraState.PROCESO.toString());
			compra = compraRepository.save(compra);
		}
		ProductoCompradoDTO actual = getProductoComprado(compra.getId(), producto.getIdProducto());
		if(actual == null) {
			actual = new ProductoCompradoDTO();
			actual.setCantidad(producto.getCantidad());
			actual.setIdCompra(compra.getId());
			actual.setIdProducto(producto.getIdProducto());
		} else {
			actual.setCantidad(actual.getCantidad() + producto.getCantidad());
		}
		inventariar(producto.getIdProducto(), producto.getCantidad()*-1);
		actual = productoCompradoRepository.save(actual);
		return consultarCarrito(usuario);
	}
	
	/**
	 * Obtenemos el carrito del usuario
	 * Cambiamos la cantidad
	 * Inventariamos la diferencia
	 * @param producto
	 * @param usuario
	 * @return
	 * @throws TiendaException
	 */
	public CarritoResponse modificarProducto(ProductoRequest producto, String usuario) throws TiendaException {
		CompraDTO compra = compraActiva(usuario);
		if(compra==null) throw new TiendaException("Seguro que quieres modificar, deberias empezar agregando un producto");
		ProductoCompradoDTO actual = getProductoComprado(compra.getId(), producto.getIdProducto());
		if(actual==null) throw new TiendaException("Este producto que quieres modificar en tu carrito no existe");
		inventariar(producto.getIdProducto(), actual.getCantidad()-producto.getCantidad());
		actual.setCantidad(producto.getCantidad());
		productoCompradoRepository.save(actual);
		return consultarCarrito(usuario);
	}
	
	/**
	 * Obtenemos los datos del carrito del usuario
	 * Obtenemos el producto, para eliminarlo del carrito
	 * inventariamos
	 * @param producto
	 * @param usuario
	 * @return
	 * @throws TiendaException
	 */
	public CarritoResponse retirarProducto(ProductoRequest producto, String usuario) throws TiendaException {
		CompraDTO compra = compraActiva(usuario);
		if(compra==null) throw new TiendaException("Me encantaria ayudarte a retirar este producto, pero da la casualidad que no tienes ningun producto");
		ProductoCompradoDTO actual = getProductoComprado(compra.getId(), producto.getIdProducto());
		if(actual==null) throw new TiendaException("Este producto que no quieres en tu carrito ya no existia");
		inventariar(producto.getIdProducto(), actual.getCantidad());
		productoCompradoRepository.delete(actual);
		return consultarCarrito(usuario);
	}
	
	
	/**
	 * Obtenemos la informacion del carrito del usuario
	 * @param usuario
	 * @return
	 * @throws TiendaException
	 */
	public CarritoResponse consultarCarrito(String usuario) throws TiendaException {
		CompraDTO compra = compraActiva(usuario);
		if(compra==null) throw new TiendaException("Agrega un producto e inicia tu compra por el momento no tienes productos");
		CarritoResponse carrito = new CarritoResponse(compra);
		carrito.setProductos(productosCarrito(compra.getId()));
		carrito.setItems(carrito.getProductos().size());
		return carrito;
	}
	
	/**
	 * Cerramos el carrito de compra e inicamos con otra compra
	 * @param usuario
	 * @return
	 * @throws TiendaException
	 */
	public CarritoResponse finalizarCompra(String usuario) throws TiendaException {
		CompraDTO compra = compraActiva(usuario);
		if(compra==null) throw new TiendaException("Intentas finalizar compra pero no tienes nada en tu carrito de compras");
		CarritoResponse carrito = consultarCarrito(usuario); 
		compra.setFechaCierre(new Date());
		compra.setEstado(CompraState.FINALIZADO.toString());
		carrito.setEstado(CompraState.FINALIZADO.toString());
		compraRepository.save(compra);
		return carrito;
	}
	
	
	/**
	 * Consultamos la compra activa o carrito del usuario
	 * @param usuario
	 * @return
	 */
	private CompraDTO compraActiva(String usuario) {
		Optional<CompraDTO> result = compraRepository.findByUsuario(usuario);
		if(result.isEmpty()) return null;
		return result.get();
	}
	
	/**
	 * Obtenemos los productos del carrito de compra
	 * @param carritoId
	 * @return
	 */
	private List<ProductoCompradoResponse> productosCarrito(String carritoId) {
		return productoCompradoRepository.findByIdCompra(carritoId).stream().map(x-> new ProductoCompradoResponse(x)).toList();
	}
	
	/**
	 * Obtenemos un solo producto de la canasta
	 * @param compra
	 * @param producto
	 * @return
	 */
	private ProductoCompradoDTO getProductoComprado(String compra, String producto) {
		Optional<ProductoCompradoDTO> result = productoCompradoRepository.findByCompraProduct(compra, producto);
		if(result.isEmpty()) return null;
		return result.get();
	}
	
	/**
	 * Inventariamos existencias, facil en el campo existencia lde producto
	 * validamos que el producto existe y lo obtenemos
	 * validamos que las existencias nunca sean negativas
	 * modificamos las existencias
	 * @param productoId
	 * @param cantidad
	 * @throws TiendaException
	 */
	private void inventariar(String productoId, int cantidad) throws TiendaException {
		if(cantidad==0) throw new TiendaException("Casi me atrapas tambien valide que no sea cero");
		Optional<ProductoDTO> productoDTO = productoRepository.findById(productoId);
		if(productoDTO.isEmpty()) throw new TiendaException("Oye desarrollador este producto no existe");
		ProductoDTO producto = productoDTO.get();
		int nuevaExistencia = producto.getExistencias() + cantidad;
		if(nuevaExistencia <0) throw new TiendaException("No hay suficientes existencias, actualmente solo tenemos " + producto.getExistencias() + " " + producto.getNombre());
		producto.setExistencias(nuevaExistencia);		
		productoRepository.save(producto);
	}
}
