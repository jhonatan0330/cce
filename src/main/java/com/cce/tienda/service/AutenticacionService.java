package com.cce.tienda.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.cce.tienda.exception.TiendaException;
import com.cce.tienda.model.dto.DatosSesionDTO;
import com.cce.tienda.model.response.TokenResponse;
import com.cce.tienda.persistence.DatosSesionRepository;

@Service
public class AutenticacionService  {

	@Autowired private DatosSesionRepository sesionRepository;
	
	public TokenResponse login(String username, String password) throws TiendaException {
		try {
			DatosSesionDTO filter = new DatosSesionDTO();
			filter.setUsername(username);
			filter.setClave(password);
			Optional<DatosSesionDTO> result = sesionRepository.findOne(Example.of(filter));
			if(result.isEmpty()) throw new TiendaException("Credenciales incorrectas o la cuenta no se encuentra activa");
			return new TokenResponse(result.get().getId());
		} catch (Exception e) {
			throw new TiendaException(e.getMessage());
		}
	}
	
	public String validateToken(String token) throws TiendaException {
		Optional<DatosSesionDTO> filter = sesionRepository.findById(token);
		if(filter.isEmpty()) throw new TiendaException("Tu token es incorrecto.");
		return filter.get().getIdUsuario();
	}

}
