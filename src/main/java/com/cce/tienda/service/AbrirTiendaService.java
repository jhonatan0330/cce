package com.cce.tienda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cce.tienda.model.dto.DatosSesionDTO;
import com.cce.tienda.model.dto.ProductoDTO;
import com.cce.tienda.model.dto.UsuarioDTO;
import com.cce.tienda.persistence.DatosSesionRepository;
import com.cce.tienda.persistence.ProductoRepository;
import com.cce.tienda.persistence.UsuarioRepository;

@Service
public class AbrirTiendaService {

	@Autowired private UsuarioRepository usuarioRepository;
	@Autowired private DatosSesionRepository datosSesionRepository;
	@Autowired private ProductoRepository productoRepository;
	
	public void abrir() {
		crearUsuarios();
		crearProductos();
	}

	private void crearProductos() {
		productoRepository.save(new ProductoDTO("Huevos"));
		productoRepository.save(new ProductoDTO("Leche"));
		productoRepository.save(new ProductoDTO("Pan"));
	}

	private void crearUsuarios() {
		datosSesionRepository.save(new DatosSesionDTO("U1",usuarioRepository.save(new UsuarioDTO("John Doe Infaltable")).getId()));
		datosSesionRepository.save(new DatosSesionDTO("U2",usuarioRepository.save(new UsuarioDTO("Mary Kate Spiderman")).getId()));
		datosSesionRepository.save(new DatosSesionDTO("U3",usuarioRepository.save(new UsuarioDTO("Jhonatan new Arquitect")).getId()));
	}
}
