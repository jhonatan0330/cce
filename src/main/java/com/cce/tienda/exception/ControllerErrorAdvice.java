package com.cce.tienda.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerErrorAdvice {
	
    @ExceptionHandler({TiendaException.class})
    public ResponseEntity<APIErrorResponse> handle(TiendaException e) {
    	 APIErrorResponse response =new APIErrorResponse.ApiErrorResponseBuilder()
    		        .withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    		        .withError_code(HttpStatus.INTERNAL_SERVER_ERROR.name())
    		        .withMessage(e.getMessage())
    		        .withDetail(e.getOrigen())
    		        .build();
    		        return new ResponseEntity<APIErrorResponse>(response, response.getStatus());
	}
    
   
   
}
