package com.cce.tienda.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cce.tienda.model.dto.CompraDTO;

@Repository
public interface CompraRepository extends JpaRepository<CompraDTO, String> {


	@Query("SELECT p FROM CompraDTO p WHERE p.idUsuario = ?1 and p.estado= 'PROCESO'")
	public Optional<CompraDTO> findByUsuario(String usuario);
	
}
