package com.cce.tienda.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cce.tienda.model.dto.DatosSesionDTO;

@Repository
public interface DatosSesionRepository extends JpaRepository<DatosSesionDTO, String> {
	
}
