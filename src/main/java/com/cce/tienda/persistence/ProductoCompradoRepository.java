package com.cce.tienda.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cce.tienda.model.dto.ProductoCompradoDTO;

@Repository
public interface ProductoCompradoRepository extends JpaRepository<ProductoCompradoDTO, String>{

	@Query("SELECT p FROM ProductoCompradoDTO p WHERE p.idCompra = ?1")
	public List<ProductoCompradoDTO> findByIdCompra(String idCompra);
	
	@Query("SELECT p FROM ProductoCompradoDTO p WHERE p.idCompra = ?1 and idProducto = ?2")
	public Optional<ProductoCompradoDTO> findByCompraProduct(String compra, String producto);
	
}
