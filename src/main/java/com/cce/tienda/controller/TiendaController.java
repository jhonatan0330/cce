package com.cce.tienda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cce.tienda.exception.TiendaException;
import com.cce.tienda.model.request.LoginRequest;
import com.cce.tienda.model.request.ProductoRequest;
import com.cce.tienda.model.response.CarritoResponse;
import com.cce.tienda.model.response.ProductoResponse;
import com.cce.tienda.model.response.TokenResponse;
import com.cce.tienda.service.AutenticacionService;
import com.cce.tienda.service.InventarioService;
import com.cce.tienda.service.TiendaService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/v1")
public class TiendaController {

	@Autowired private AutenticacionService autenticacionService;
	@Autowired private InventarioService inventarioService;
	@Autowired private TiendaService tiendaService;

	@Operation(summary = "Primero vas a necesitar ingresar a la tienda coloca tu username y clave, te ayudo puede ser U1 con clave 123, no le digas a nadie")
	@RequestMapping(value = "/1_login", method = RequestMethod.POST)
	public TokenResponse login(@RequestBody LoginRequest loginRequest) throws TiendaException {
		return autenticacionService.login(loginRequest.getUsername(), loginRequest.getPassword());
	}

	@Operation(summary = "Ahora te ofrecemos una gran cantidad de productos y de la mejor calidad, pero por hoy solo tenemos estos disponibles")
	@RequestMapping(value = "/2_catalogo", method = RequestMethod.GET)
	public List<ProductoResponse> listarProductos() throws TiendaException {
		return inventarioService.listarInventario();
	}
	
	@Operation(summary = "Ya que te antojaste de llevar un producto, agregalo al carrito")
	@RequestMapping(value = "/3_agregar", method = RequestMethod.POST)
	public CarritoResponse agregarProducto(@RequestHeader("autorization") String token, @RequestBody ProductoRequest producto) throws TiendaException {
		String usuario = autenticacionService.validateToken(token);
		return tiendaService.agregarProducto(producto, usuario);
	}
	
	@Operation(summary = "Espero que modifiques el producto para comprar mas cantidades")
	@RequestMapping(value = "/4_modificar", method = RequestMethod.PUT)
	public CarritoResponse modificarProducto(@RequestHeader("autorization") String token, @RequestBody ProductoRequest producto) throws TiendaException {
		String usuario = autenticacionService.validateToken(token);
		return tiendaService.modificarProducto(producto, usuario);
	}
	
	@Operation(summary = "Estas seguro, no deseas quedarte sin un producto y llegar a casa sin el")
	@RequestMapping(value = "/5_retirar", method = RequestMethod.DELETE)
	public CarritoResponse retirarProducto(@RequestHeader("autorization") String token, @RequestBody ProductoRequest producto) throws TiendaException {
		String usuario = autenticacionService.validateToken(token);
		return tiendaService.retirarProducto(producto, usuario);
	}
	
	@Operation(summary = "Aqui puedes ver los productos que has agregado")
	@RequestMapping(value = "/6_carrito", method = RequestMethod.GET)
	public CarritoResponse consultarCarrito(@RequestHeader("autorization") String token) throws TiendaException {
		String usuario = autenticacionService.validateToken(token);
		return tiendaService.consultarCarrito(usuario);
	}
	
	@Operation(summary = "Ahora si vamonos a casa, con esto ya finalizas")
	@RequestMapping(value = "/7_pagar", method = RequestMethod.POST)
	public CarritoResponse finalizarCompra(@RequestHeader("autorization") String token) throws TiendaException {
		String usuario = autenticacionService.validateToken(token);
		return tiendaService.finalizarCompra(usuario);
	}
}
